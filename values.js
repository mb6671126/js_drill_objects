function values(obj) {
    try{
    const objValues = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key) && typeof obj[key] !== 'function') {
            objValues.push(obj[key]);
        }
    }
    return objValues;
    }catch(error){
        console.error('Error is :',error.message);
    }
}

module.exports= values;