const defaults = require('../default.js');
// for devault function

const defaultProps = { age: 30, superhero : 'Batman'};// default object

const  objects= require('../Dataset.js');

try{

    console.log("\nApplying defaults to the Object:");
    const objWithDefaults = defaults(objects, defaultProps);
    console.log(objWithDefaults);

  }catch (error) {

    console.error("Error:", error.message);
  
  }
