const object=require('../Dataset.js');

const mapObject= require('../mapObject.js');

try{
      // the values that are alphabetic is changed to uppercase.   
    const uppercaseObject = mapObject(object, value => {
        if (typeof value === 'string') {
            return value.toUpperCase();
        }
        return value;
    });
     console.log(uppercaseObject);

     // Have  doubled the age
    const doubledAgeObject = mapObject(object, value => {
     if (typeof value === 'number') {
         return value * 2;
      }
      return value;
    });
    console.log(doubledAgeObject);
  } catch (error) {
      console.error("Error:", error.message);
  }