
function defaults(obj, defaultProps) {

try{
    for (let key in defaultProps) {
        if (defaultProps.hasOwnProperty(key) && obj[key] === undefined) {
            obj[key] = defaultProps[key];
        }
    }
    return obj;
  }catch(error){
        console.error('Error is :',error.message); 
  }
}

module.exports= defaults;